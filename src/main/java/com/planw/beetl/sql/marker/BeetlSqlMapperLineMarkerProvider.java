package com.planw.beetl.sql.marker;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.openapi.editor.markup.GutterIconRenderer;
import com.intellij.psi.*;
import com.planw.beetl.utils.ContextIcons;
import com.planw.beetl.utils.JavaService;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class BeetlSqlMapperLineMarkerProvider extends RelatedItemLineMarkerProvider {

    @Override
    protected void collectNavigationMarkers(@NotNull PsiElement element, @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {
        if (!(element instanceof PsiClass)) {
            return;
        }
        JavaService javaService = JavaService.getInstance(element.getProject());
        PsiClass psiClass = (PsiClass) element;
        if (!psiClass.isInterface() || !javaService.checkIsBeetlSqlClass(psiClass)) {
            return;
        }
		String sqlFileName = null;
        PsiClassType[] classTypes = psiClass.getExtendsListTypes();
		PsiAnnotation sqlResource = psiClass.getAnnotation("org.beetl.sql.mapper.annotation.SqlResource");
		if(sqlResource!=null){
			PsiAnnotationMemberValue value = sqlResource.findDeclaredAttributeValue("value");
			sqlFileName = value.getText();
			//去掉俩边双引号
			sqlFileName = sqlFileName.substring(1,sqlFileName.length()-1);
		}else{
			for (PsiClassType classType : classTypes) {
				if (!classType.getInternalCanonicalText().startsWith("org.beetl.sql.mapper.BaseMapper")) {
					continue;
				}
				sqlFileName = classType.getParameters()[0].getPresentableText();
			}
		}


        if (StringUtils.isBlank(sqlFileName)) {
            return;
        }
        PsiMethod[] psiMethods = psiClass.getMethods();
        if (psiMethods == null || psiMethods.length < 1) {
            return;
        }
        Map<String, PsiMethod> psiMethodMap = new HashMap<>();
        for (PsiMethod psiMethod : psiMethods) {
            psiMethodMap.put(psiMethod.getName(), psiMethod);
        }
        
        List<PsiFile> fileList = javaService.getAllBeetlSqlFiles(psiClass.getProject(), sqlFileName);
        if (fileList == null || fileList.isEmpty()) {
            return;
        }
        Map<String, List<PsiElement>> targetSqlElement = javaService.buildTargetPsiElement(fileList, psiMethodMap);
        for (Map.Entry<String, PsiMethod> entry : psiMethodMap.entrySet()) {
            String methodName = entry.getKey();
            if (!targetSqlElement.containsKey(methodName)) {
                continue;
            }

            List<PsiElement> targetSqlElList = targetSqlElement.get(methodName);
            if (targetSqlElList == null || targetSqlElList.isEmpty()) {
                continue;
            }

            // 过滤重复
            List<PsiElement> newSetElement = new LinkedList<>();
            Set<Integer> haveKeys = new HashSet<>();
            for (PsiElement psiElement : targetSqlElList) {
                if(haveKeys.contains(Integer.valueOf(psiElement.getTextOffset()))){
                    continue;
                }
                haveKeys.add(Integer.valueOf(psiElement.getTextOffset()));
                newSetElement.add(psiElement);
            }
       

            PsiMethod psiMethod = entry.getValue();

            NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(ContextIcons.MAPPER_LINE_MARKER_ICON)
                    .setAlignment(GutterIconRenderer.Alignment.CENTER)
                    .setTargets(newSetElement)
                    .setTooltipTitle("Navigation to target in beetlsql "+newSetElement.get(0).getText());
            result.add(
                    builder.createLineMarkerInfo(((PsiNameIdentifierOwner) psiMethod).getNameIdentifier())
            );
        }
    }


}
