package com.planw.beetl.sql;

import com.intellij.navigation.ChooseByNameContributorEx;
import com.intellij.navigation.NavigationItem;
import com.intellij.openapi.project.Project;
import com.intellij.psi.search.EverythingGlobalScope;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.ProjectScope;
import com.intellij.util.ArrayUtilRt;
import com.intellij.util.Processor;
import com.intellij.util.indexing.FindSymbolParameters;
import com.intellij.util.indexing.IdFilter;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class SimpleChooseByNameContributor implements ChooseByNameContributorEx {


  @Override
  public void processNames(@NotNull Processor<? super String> processor,
      @NotNull GlobalSearchScope globalSearchScope, @Nullable IdFilter idFilter) {

  }

  @Override
  public void processElementsWithName(@NotNull String s,
      @NotNull Processor<? super NavigationItem> processor,
      @NotNull FindSymbolParameters findSymbolParameters) {

  }


  /**
   * @deprecated Use {@link #processNames(Processor, GlobalSearchScope, IdFilter)} instead
   */
  @SuppressWarnings("all")
  @Override
  public String @NotNull [] getNames(Project project, boolean includeNonProjectItems) {

    List<String> result = new ArrayList<>();
    processNames(result::add, FindSymbolParameters.searchScopeFor(project, includeNonProjectItems),
        null);
    return ArrayUtilRt.toStringArray(result);
  }

  /**
   * @deprecated Use {@link #processElementsWithName(String, Processor, FindSymbolParameters)}
   * instead
   */
  @Override
  public NavigationItem @NotNull [] getItemsByName(String name, String pattern, Project project,
      boolean includeNonProjectItems) {

    List<NavigationItem> result = new ArrayList<>();
    GlobalSearchScope globalSearchScope = project == null ? new EverythingGlobalScope() :
        includeNonProjectItems ? ProjectScope.getAllScope(project) :
            ProjectScope.getProjectScope(project);
    FindSymbolParameters findSymbolParameters = new FindSymbolParameters("", "", globalSearchScope,
        IdFilter.getProjectIdFilter(project, false));
    processElementsWithName(name, result::add, findSymbolParameters);
    return result.isEmpty() ? NavigationItem.EMPTY_NAVIGATION_ITEM_ARRAY
        : result.toArray(NavigationItem.EMPTY_NAVIGATION_ITEM_ARRAY);
  }

}