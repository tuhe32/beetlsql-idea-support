package com.planw.beetl.utils;

import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtil;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import org.jetbrains.idea.maven.project.MavenProject;
import org.jetbrains.idea.maven.project.MavenProjectsManager;

public class MavenUtil {

  public void getResourcePath(Project project, PsiFile containingFile) {

    Module module = ModuleUtil.findModuleForFile(containingFile);
    MavenProject mavenProject = MavenProjectsManager.getInstance(project).findProject(module);
  }

}
