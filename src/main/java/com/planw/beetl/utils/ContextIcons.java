package com.planw.beetl.utils;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class ContextIcons {
    public static final Icon BEETL_LANGUAGE = IconLoader.getIcon("/planw_images/beetl_language.png");

    public static final Icon STATEMENT_LINE_MARKER_ICON = IconLoader.getIcon("/planw_images/statement.png");

    public static final Icon MAPPER_LINE_MARKER_ICON = IconLoader.getIcon("/planw_images/mapper_method.png");
}
