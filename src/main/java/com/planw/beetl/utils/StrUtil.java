package com.planw.beetl.utils;

import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.lang3.StringUtils;

public class StrUtil {

  public static boolean searchCharSequence(String str, String search) {

    if (StringUtils.equals(search, "*")) {
      return true;
    }
    str = StringUtils.lowerCase(str);
    search = StringUtils.lowerCase(search);
    AtomicInteger postion = new AtomicInteger(0);
    String finalStr = str;
    search.chars().forEach(ch -> {
      if (postion.intValue() != -1) {
        postion.set(finalStr.indexOf(ch, postion.get()));
      }
    });

    return postion.intValue() != -1;
  }

}
