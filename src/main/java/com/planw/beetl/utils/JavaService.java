package com.planw.beetl.utils;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.psi.*;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author yanglin
 */
public class JavaService {

    private Project project;

    private JavaPsiFacade javaPsiFacade;

    public JavaService(Project project) {
        this.project = project;
        this.javaPsiFacade = JavaPsiFacade.getInstance(project);
    }

    public static JavaService getInstance(@NotNull Project project) {
        return ServiceManager.getService(project, JavaService.class);
    }


    public boolean checkIsBeetlSqlClass(PsiClass psiClass) {
        if(psiClass.getName().endsWith("Mapper")){
            return true;
        }
        PsiClass[] interFaces = psiClass.getInterfaces();
        for (PsiClass interFace : interFaces) {

            if (interFace.getQualifiedName().equalsIgnoreCase("org.beetl.sql.core.mapper.BaseMapper")) {
                return true;
            }
        }
        return false;
    }

    public Map<String, List<PsiElement>> buildTargetPsiElement(List<PsiFile> psiFileList, Map<String, PsiMethod> psiMethodMap) {
        Map<String, List<PsiElement>> listMap = new HashMap<>();
        for (PsiFile psiFile : psiFileList) {
            List<PsiElement> psiElNeed = new LinkedList<>();
            buildFilePsiElements(psiFile.getChildren(), listMap, psiMethodMap);
        }
        return listMap;
    }

    protected void buildFilePsiElements(PsiElement[] tempEls, Map<String, List<PsiElement>> listMap, Map<String, PsiMethod> psiMethodMap) {
        for (PsiElement tempEl : tempEls) {
            String sqlId = StringUtils.trim(tempEl.getText());
            if (psiMethodMap.containsKey(sqlId)) {
                List<PsiElement> elementList = listMap.get(sqlId);
                if (elementList == null) {
                    elementList = new LinkedList<>();
                    listMap.put(sqlId, elementList);
                }
                elementList.add(tempEl);
            }
            PsiElement[] tempElsCl = tempEl.getChildren();
            if (tempElsCl != null && tempElsCl.length > 0) {
                buildFilePsiElements(tempElsCl, listMap, psiMethodMap);
            }
        }
    }


    public List<PsiFile> getAllBeetlSqlFiles(Project project, String className) {
        List<PsiFile> psiFileList = new LinkedList<>();
        String sqlFileName = StringUtils.uncapitalize(className) + ".sql";
        loadPsiFiles(project, sqlFileName, psiFileList);
        sqlFileName = StringUtils.uncapitalize(className) + ".md";
        loadPsiFiles(project, sqlFileName, psiFileList);
        return psiFileList;
    }

    public void loadPsiFiles(Project project, String sqlFileName, List<PsiFile> psiFiles) {
        PsiFile[] files = FilenameIndex.getFilesByName(project, sqlFileName, GlobalSearchScope.allScope(project));
        for (PsiFile file : files) {
            psiFiles.add(file);
        }
    }
    
}

