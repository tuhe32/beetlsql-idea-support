关于开发idea插件时下载idea的代码的快速方法

由于国内访问国外的网络的问题，gradle无法直接下载。
我们可以通过自己下载的方法然后让gradle自行解压。

第一：gradle在下载idea.zip时会在下面的目录中
gradle_repo\caches\modules-2\files-2.1\com.jetbrains.intellij.idea\ideaIC\212.4746.92\4ff7516024b4b547dc65b07c9a6763f2d6b6a627

第二：可以等到上述目录生成后中止下载，然后将我们下载的zip放进去，然后重新载入gradle即可。

下载网站：[Idea 各版本下载网站](https://www.jetbrains.com/intellij-repository/releases)